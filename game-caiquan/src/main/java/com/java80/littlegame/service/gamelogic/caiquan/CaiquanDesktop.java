package com.java80.littlegame.service.gamelogic.caiquan;

import java.util.ArrayList;
import java.util.List;

import com.java80.littlegame.common.db.dao.DAOFactory;
import com.java80.littlegame.common.db.dao.UUserInfoDao;
import com.java80.littlegame.common.db.entity.UUserInfo;
import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;
import com.java80.littlegame.common.message.proto.cluster.ClusterMessage;
import com.java80.littlegame.common.message.proto.game.caiquan.CaiquanActionMessage;
import com.java80.littlegame.common.message.proto.game.caiquan.CaiquanActionResultMessage;
import com.java80.littlegame.common.message.proto.game.caiquan.CaiquanSettleMessage;
import com.java80.littlegame.common.message.proto.game.caiquan.StartCaiquanGameMessage;
import com.java80.littlegame.service.game.desk.DeskSetting;
import com.java80.littlegame.service.game.desk.Desktop;

public class CaiquanDesktop extends Desktop {
	private CaiquanGameInfo gameInfo;

	public CaiquanDesktop(DeskSetting setting) {
		super(setting);
	}

	@Override
	public void onStart() {
		gameInfo = new CaiquanGameInfo();
		int gameId = setting.getGameId();
		int roomId = setting.getRoomId();
		List<Long> playerIds = setting.getPlayerIds();
		List<CaiquanPlayer> players = new ArrayList<>();
		StartCaiquanGameMessage start = new StartCaiquanGameMessage();
		UUserInfoDao userInfoDao = DAOFactory.getUserInfoDao();
		List<StartCaiquanGameMessage.PlayerInfo> infos = new ArrayList<>();
		for (long pid : playerIds) {
			CaiquanPlayer p = new CaiquanPlayer();
			UUserInfo info = null;
			try {
				info = userInfoDao.get(pid);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			p.setNickName(info.getNickName());
			p.setPlayerId(pid);
			players.add(p);
			StartCaiquanGameMessage.PlayerInfo inf = new StartCaiquanGameMessage.PlayerInfo();
			inf.setId(pid);
			inf.setNickName(info.getNickName());
			infos.add(inf);
		}
		gameInfo.setPlayers(players);
		gameInfo.setActionPlayers(new ArrayList<>(players));// 两个人都可以出拳
															// 没有先后顺序，根据汇总在服务器的数据结算

		start.setGameId(gameId);
		start.setRoomId(roomId);
		start.setInfos(infos);
		// 发送开局消息给客户端
		broadcastMessage(start);
		// 告诉客户端可以开始动作了,客户端收到

		CaiquanActionMessage msg = new CaiquanActionMessage();
		msg.setActionId(ActionConsts.ACTION_CHUQUAN);
		broadcastMessage(msg);
	}

	@Override
	public void onMessage(long userId, BaseMsg msg) {
		CaiquanPlayer player = gameInfo.findPlayer(userId);
		if (player != null) {
			switch (msg.getCode()) {
			case ProtoList.MSG_CODE_CAIQUANACTION:
				caiquan(player, (CaiquanActionMessage) msg);
				break;

			default:
				break;
			}
		}

	}

	private void caiquan(CaiquanPlayer player, CaiquanActionMessage msg) {
		/// 如果两个人都出拳了再公布结果
		player.setChuquan(msg.getResult());
		gameInfo.getActionPlayers().remove(player);
		if (gameInfo.getActionPlayers().isEmpty()) {
			CaiquanActionResultMessage rmsg = new CaiquanActionResultMessage();
			rmsg.setActionId(ActionConsts.ACTION_CHUQUAN);
			List<CaiquanActionResultMessage.Result> rs = new ArrayList<>();
			for (CaiquanPlayer p : gameInfo.getPlayers()) {
				CaiquanActionResultMessage.Result r = new CaiquanActionResultMessage.Result();
				r.setResult(p.getChuquan());
				r.setUserId(p.getPlayerId());
				rs.add(r);
			}
			rmsg.setResults(rs);
			broadcastMessage(rmsg);
			// 结算输赢
			settle();
		}

	}

	private void settle() {
		// 找到赢家 两个人比大小
		CaiquanPlayer p1 = gameInfo.getPlayers().get(0);
		CaiquanPlayer p2 = gameInfo.getPlayers().get(1);
		byte c1 = p1.getChuquan();
		byte c2 = p2.getChuquan();
		CaiquanPlayer winner = null;
		if (c1 == ActionConsts.RESULT_SHITOU) {
			if (c2 == ActionConsts.RESULT_JIANDAO) {
				winner = p1;
			} else if (c2 == ActionConsts.RESULT_BU) {
				winner = p2;
			}
		} else if (c1 == ActionConsts.RESULT_JIANDAO) {
			if (c2 == ActionConsts.RESULT_BU) {
				winner = p1;
			} else if (c2 == ActionConsts.RESULT_SHITOU) {
				winner = p2;
			}
		} else if (c1 == ActionConsts.RESULT_BU) {
			if (c2 == ActionConsts.RESULT_SHITOU) {
				winner = p1;
			} else if (c2 == ActionConsts.RESULT_JIANDAO) {
				winner = p2;
			}
		}
		CaiquanSettleMessage sm = new CaiquanSettleMessage();
		sm.setWinner(winner == null ? 0 : winner.getPlayerId());
		broadcastMessage(sm);
		endDesk();
	}

	@Override
	public void onClusterMessage(ClusterMessage msg) {
		// TODO Auto-generated method stub

	}

}
