package com.java80.littlegame.service.gateway.websocket;

import com.java80.littlegame.common.base.BaseHandler;
import com.java80.littlegame.common.base.GameUser;
import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoHelper;
import com.java80.littlegame.common.message.proto.ProtoList;
import com.java80.littlegame.common.message.proto.hall.UserLoginMessage;
import com.java80.littlegame.service.gateway.session.ChannelManageCenter;

public class GatewayHandler extends BaseHandler {

	public void onMessage(BaseMsg msg) {
		switch (msg.getType()) {
		case ProtoList.MSG_TYPE_HALL:
			processHallMsg(msg);
			break;
		default:
			MessageHelper.outMsg(msg);
			break;
		}

	}

	private void processHallMsg(BaseMsg msg) {
		switch (msg.getCode()) {
		case ProtoList.MSG_CODE_LOGIN:
			doLogin((UserLoginMessage) msg);
			break;
		default:
			MessageHelper.outMsg(msg);
			break;
		}

	}

	private void doLogin(UserLoginMessage msg) {
		int result = msg.getResult();
		// 登录之后的消息是多播的，需要鉴别
		long userId = msg.getRecUserId();
		GameUser gameUser = GameUser.getGameUser(userId);

		if (result == 1) {
			boolean bind = ChannelManageCenter.getInstance().bind(msg.getSessionId(), userId);
			if (bind) {
				gameUser.setGatewayServiceId(GatewayConfig.getServiceId());
				gameUser.setSessionId(msg.getSessionId());
				gameUser.save();
			}
		}
		MessageHelper.outMsg(msg);
	}

	@Override
	public Object onReceive(String obj) {
		if (obj instanceof String) {
			onMessage(ProtoHelper.parseJSON(obj.toString()));
		}
		return null;
	}

}
