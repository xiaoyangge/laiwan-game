/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : littlegame

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 13/04/2018 18:38:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for s_game_config
-- ----------------------------
DROP TABLE IF EXISTS `s_game_config`;
CREATE TABLE `s_game_config`  (
  `gameId` int(11) NOT NULL,
  `playerSum` int(255) NULL DEFAULT NULL COMMENT '这个游戏要多少人参与',
  `expride` int(255) NULL DEFAULT NULL COMMENT '超过多少秒解散房间',
  `desktopClass` varchar(255) NULL DEFAULT NULL COMMENT '游戏的主类',
  PRIMARY KEY (`gameId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '游戏配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of s_game_config
-- ----------------------------
INSERT INTO `s_game_config` VALUES (1001, 2, 86400);

-- ----------------------------
-- Table structure for s_game_info
-- ----------------------------
DROP TABLE IF EXISTS `s_game_info`;
CREATE TABLE `s_game_info`  (
  `id` int(11) NOT NULL,
  `gameName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '游戏信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of s_game_info
-- ----------------------------
INSERT INTO `s_game_info` VALUES (1001, '猜拳游戏', '2018-04-12 15:36:37');

-- ----------------------------
-- Table structure for u_room_info
-- ----------------------------
DROP TABLE IF EXISTS `u_room_info`;
CREATE TABLE `u_room_info`  (
  `roomId` int(11) NOT NULL COMMENT '房间id',
  `gameId` int(11) NULL DEFAULT NULL,
  `password` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL COMMENT '0无状态1锁定不可加入2以开始',
  `userId` bigint(20) NOT NULL COMMENT '房主',
  `createTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`roomId`, `userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '房间信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of u_room_info
-- ----------------------------
INSERT INTO `u_room_info` VALUES (658651, 1001, 9137, 0, 1, '2018-04-13 10:49:29');

-- ----------------------------
-- Table structure for u_user_info
-- ----------------------------
DROP TABLE IF EXISTS `u_user_info`;
CREATE TABLE `u_user_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nickName` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `loginName` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of u_user_info
-- ----------------------------
INSERT INTO `u_user_info` VALUES (1, 'ace', 'ace', '202cb962ac59075b964b07152d234b70', '2018-03-30 11:50:56');
INSERT INTO `u_user_info` VALUES (2, 'ace', '33', '33', '2018-03-30 11:51:04');
INSERT INTO `u_user_info` VALUES (3, 'haiya', 'ace1', '202cb962ac59075b964b07152d234b70', '2018-04-13 15:21:22');

-- ----------------------------
-- Table structure for u_user_room_info
-- ----------------------------
DROP TABLE IF EXISTS `u_user_room_info`;
CREATE TABLE `u_user_room_info`  (
  `userId` bigint(20) NOT NULL,
  `roomId` int(11) NOT NULL,
  `gameId` int(11) NULL DEFAULT NULL,
  `createTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`userId`, `roomId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户在哪个房间表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of u_user_room_info
-- ----------------------------
INSERT INTO `u_user_room_info` VALUES (1, 658651, 1001, '2018-04-13 10:49:29');
INSERT INTO `u_user_room_info` VALUES (3, 658651, 1001, '2018-04-13 15:28:48');

SET FOREIGN_KEY_CHECKS = 1;
