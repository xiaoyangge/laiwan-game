package com.java80.littlegame.common.base;

import java.io.Serializable;

import com.java80.littlegame.common.cache.CacheObj;
import com.java80.littlegame.common.cache.CacheService;

public class GameUser extends CacheObj implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7312914784693240088L;
	private static final String GAMEUSER_SAVE_KEY = "gameuser";
	private long userId;

	private String gatewayServiceId;
	private String hallServiceId;
	private String quartzServiceId;
	private String roomServiceId;
	private String gameServiceId;
	private String sessionId;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getGameServiceId() {
		return gameServiceId;
	}

	public void setGameServiceId(String gameServiceId) {
		this.gameServiceId = gameServiceId;
	}

	public String getRoomServiceId() {
		return roomServiceId;
	}

	public void setRoomServiceId(String roomServiceId) {
		this.roomServiceId = roomServiceId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getGatewayServiceId() {
		return gatewayServiceId;
	}

	public void setGatewayServiceId(String gatewayServiceId) {
		this.gatewayServiceId = gatewayServiceId;
	}

	public String getHallServiceId() {
		return hallServiceId;
	}

	public void setHallServiceId(String hallServiceId) {
		this.hallServiceId = hallServiceId;
	}

	public String getQuartzServiceId() {
		return quartzServiceId;
	}

	public void setQuartzServiceId(String quartzServiceId) {
		this.quartzServiceId = quartzServiceId;
	}

	public static GameUser getGameUser(long userId) {
		// 从缓存中取，
		GameUser gameUser = CacheService.get(makeKeyName(GAMEUSER_SAVE_KEY + userId), GameUser.class);
		return gameUser;
	}

	@Override
	public String toString() {
		return "GameUser [userId=" + userId + ", gatewayServiceId=" + gatewayServiceId + ", hallServiceId="
				+ hallServiceId + ", quartzServiceId=" + quartzServiceId + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GameUser) {
			return this.getUserId() == ((GameUser) obj).getUserId();
		}
		return false;

	}

	@Override
	public String getKeyName() {
		return GAMEUSER_SAVE_KEY + getUserId();
	}

}
