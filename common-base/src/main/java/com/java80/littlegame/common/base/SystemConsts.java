package com.java80.littlegame.common.base;

public class SystemConsts {
	public static final int SERVICE_REFRESH_TIME = 50000;

	public static final int SERVICE_TYPE_GATEWAY = 1;
	public static final int SERVICE_TYPE_HALL = 2;
	public static final int SERVICE_TYPE_QUARTZ = 3;
	public static final int SERVICE_TYPE_ROOM = 4;
	public static final int SERVICE_TYPE_GAME = 5;

	public static final String EXCHANGE_NAME_SERVICE_REFRESH = "exchange.refresh.service";
	public static final String SERVICE_STATUS_KEY = "cache.service.status";
	public static final String SERVICE_STATUS_MAP_KEY = "cache.service.status";
	public static final int CACHE_DB_ID = 6;
	public static final String GLOBAL_CONFIG_NAME = "global.config.name";
	public static final String GLOBAL_SYSTEM_CONFIG = "global.system.config.name";
	public static final String GLOBAL_SYSTEM_CONFIG_GAMEINFO = "global.system.config.gameinfo";
	public static final int REDIS_TTL = 24 * 3600;
	public static final String SERVICE_ID_KEY = "service.id";
}
