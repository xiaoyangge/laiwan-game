package com.java80.littlegame.common.message.proto.hall;

import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;

public class UserLoginMessage extends BaseMsg {
	private int result;
	private String loginName;
	private String password;

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_HALL;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_LOGIN;
	}

}
