package com.java80.littlegame.common.db.utils;

import java.sql.Connection;
import java.util.Properties;

import com.alibaba.druid.pool.DruidDataSource;
import com.java80.littlegame.common.utils.LoadPropertiesFileUtil;

public class DBConnectionFactory {
	private static DruidDataSource dataSource = null;
	static {
		init();
	}

	public static void init() {
		Properties p = LoadPropertiesFileUtil.loadProperties("../config/cfg.properties");
		dataSource = new DruidDataSource();
		dataSource.setDriverClassName(p.getProperty("base.db.driver"));
		dataSource.setUrl(p.getProperty("base.db.url"));
		dataSource.setUsername(p.getProperty("base.db.user"));
		dataSource.setPassword(p.getProperty("base.db.pwd"));
		dataSource.setInitialSize(Integer.parseInt(p.getProperty("base.db.initsize")));
		dataSource.setMinIdle(1);
		dataSource.setMaxActive(Integer.parseInt(p.getProperty("base.db.maxactive")));
		dataSource.setPoolPreparedStatements(false);
	}

	public static synchronized Connection getConnection() {
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}
