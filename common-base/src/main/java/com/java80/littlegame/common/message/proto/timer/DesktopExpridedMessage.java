package com.java80.littlegame.common.message.proto.timer;

import com.java80.littlegame.common.message.proto.ProtoList;

public class DesktopExpridedMessage extends TimerMessage {
	private int gameId;
	private int roomId;

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated stub
		return ProtoList.MSG_CODE_TIMER_DESKTOPEXPRIDED;
	}

}
