package com.java80.littlegame.common.message.proto.game.caiquan;

import java.util.ArrayList;
import java.util.List;

import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;

public class StartCaiquanGameMessage extends BaseMsg {
	private List<PlayerInfo> infos = new ArrayList<>();
	private int gameId;
	private int roomId;

	public List<PlayerInfo> getInfos() {
		return infos;
	}

	public void setInfos(List<PlayerInfo> infos) {
		this.infos = infos;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_GAME;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_CAIQUANGAMESTART;
	}

	public static class PlayerInfo {
		private long id;
		private String nickName;

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getNickName() {
			return nickName;
		}

		public void setNickName(String nickName) {
			this.nickName = nickName;
		}
	}
}
