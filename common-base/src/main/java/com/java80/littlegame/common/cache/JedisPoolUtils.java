package com.java80.littlegame.common.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisPoolUtils {
	final transient static Logger log = LoggerFactory.getLogger(JedisPoolUtils.class);
	private static JedisPool cachedPool;
	static {
		start();
	}

	public static void start() {
		JedisPoolConfig config = new JedisPoolConfig();// Jedis池配置

		config.setMaxTotal(500); // 最大活动的对象个数

		config.setMaxIdle(1000 * 60);// 对象最大空闲时间

		config.setMaxWaitMillis(1000 * 3);// 获取对象时最大等待时间
		String url = CacheConfig.getUrl();
		int port = CacheConfig.getPort();
		int database = CacheConfig.getDatabase();
		String passwd = CacheConfig.getPwd();
		if (passwd.equals("")) {
			cachedPool = new JedisPool(config, url, port);
		} else {
			cachedPool = new JedisPool(config, url, port, 30, passwd);
		}
		log.info(cachedPool.getResource().ping());
		log.info(" * connect CacheObjManager cache " + url + " db " + database);
	}

	public static JedisPool getPool() {
		return cachedPool;
	}

}
