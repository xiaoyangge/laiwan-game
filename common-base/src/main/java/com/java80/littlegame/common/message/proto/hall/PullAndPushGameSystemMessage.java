package com.java80.littlegame.common.message.proto.hall;

import java.util.ArrayList;
import java.util.List;

import com.java80.littlegame.common.db.entity.SGameInfo;
import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;

/**
 * 客户端请求推送消息 server<->client
 * 
 * @author
 *
 */
public class PullAndPushGameSystemMessage extends BaseMsg {
	private List<SGameInfo> gameInfos = new ArrayList<>();

	public List<SGameInfo> getGameInfos() {
		return gameInfos;
	}

	public void setGameInfos(List<SGameInfo> gameInfos) {
		this.gameInfos = gameInfos;
	}

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_HALL;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_PULLANDPUSHGAMESYSTEM;
	}

}
