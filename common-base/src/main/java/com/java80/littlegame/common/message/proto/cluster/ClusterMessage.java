package com.java80.littlegame.common.message.proto.cluster;

import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;

public abstract class ClusterMessage extends BaseMsg {

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_CLUSTER;
	}
}
