package com.java80.littlegame.common.message.proto.room;

import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;

public class JoinRoomMessage extends BaseMsg {

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_ROOM;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_JION_ROOM;
	}

	private int gameId;
	private int roomId;
	private int result;
	private int password;

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getPassword() {
		return password;
	}

	public void setPassword(int password) {
		this.password = password;
	}

}
