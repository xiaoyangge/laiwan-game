package com.java80.littlegame.service.hall;

import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

import com.java80.littlegame.common.base.BaseHandler;
import com.java80.littlegame.common.base.GameUser;
import com.java80.littlegame.common.base.ServiceStatusHelper;
import com.java80.littlegame.common.base.SystemConsts;
import com.java80.littlegame.common.base.SystemDataMgr;
import com.java80.littlegame.common.base.VertxMessageHelper;
import com.java80.littlegame.common.db.dao.DAOFactory;
import com.java80.littlegame.common.db.dao.UUserInfoDao;
import com.java80.littlegame.common.db.entity.SGameInfo;
import com.java80.littlegame.common.db.entity.UUserInfo;
import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoHelper;
import com.java80.littlegame.common.message.proto.ProtoList;
import com.java80.littlegame.common.message.proto.hall.PullAndPushGameSystemMessage;
import com.java80.littlegame.common.message.proto.hall.UserLoginMessage;
import com.java80.littlegame.common.message.proto.hall.UserRegisterMessage;
import com.java80.littlegame.common.utils.MD5Security;

public class HallHandler extends BaseHandler {

	public void onMessage(BaseMsg msg) {
		switch (msg.getCode()) {
		case ProtoList.MSG_CODE_LOGIN:
			doLogin((UserLoginMessage) msg);
			break;
		case ProtoList.MSG_CODE_REGISTER:
			doRegister((UserRegisterMessage) msg);
			break;
		case ProtoList.MSG_CODE_PULLANDPUSHGAMESYSTEM:
			pullAndpushgamesystem((PullAndPushGameSystemMessage) msg);
			break;
		default:
			break;
		}
	}

	private void doRegister(UserRegisterMessage msg) {
		// 检查登录名
		String loginName = msg.getLoginName();
		UUserInfoDao userInfoDao = DAOFactory.getUserInfoDao();
		try {
			if (userInfoDao.findByLoginName(loginName) == null) {
				msg.setResult(1);
				String nickName = msg.getNickName();
				String password = msg.getPassword();
				try {
					String pwd = MD5Security.md5hex32(password);
					UUserInfo u = new UUserInfo();
					u.setCreateTime(new Date());
					u.setLoginName(loginName);
					u.setNickName(nickName);
					u.setPassword(pwd);
					userInfoDao.insert(u);
				} catch (Exception e) {
					e.printStackTrace();
					msg.setResult(-2);
				}
			} else {
				msg.setResult(-1);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		VertxMessageHelper.broadcastMessageToService(SystemConsts.SERVICE_TYPE_GATEWAY, msg);
	}

	private void pullAndpushgamesystem(PullAndPushGameSystemMessage msg) {
		TreeMap<Integer, SGameInfo> gameInfos = SystemDataMgr.getGameInfos();
		msg.setGameInfos(new ArrayList<>(gameInfos.values()));
		long userId = msg.getSenderUserId();
		GameUser gameUser = GameUser.getGameUser(userId);
		VertxMessageHelper.sendMessageToService(
				ServiceStatusHelper.findServiceStatus(gameUser.getGatewayServiceId()).getServiceQueueName(),
				msg.toString());
	}

	private void doLogin(UserLoginMessage msg) {
		String password = msg.getPassword();
		String userName = msg.getLoginName();
		try {
			password = MD5Security.md5hex32(password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		UUserInfoDao dao = DAOFactory.getUserInfoDao();
		UUserInfo userInfo = null;
		try {
			userInfo = dao.login(userName, password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (userInfo != null) {
			msg.setResult(1);
			msg.setRecUserId(userInfo.getId());
			// 查找可用service,随机分配room节点和game节点
			GameUser gu = new GameUser();
			gu.setHallServiceId(HallConfig.getServiceId());
			gu.setUserId(userInfo.getId());
			gu.setRoomServiceId(ServiceStatusHelper.randServiceStatus(SystemConsts.SERVICE_TYPE_ROOM).getServiceId());
			gu.setGameServiceId(ServiceStatusHelper.randServiceStatus(SystemConsts.SERVICE_TYPE_GAME).getServiceId());
			gu.save();
		} else {
			msg.setResult(0);

		}
		VertxMessageHelper.broadcastMessageToService(SystemConsts.SERVICE_TYPE_GATEWAY, msg);
	}

	@Override
	public Object onReceive(String obj) {
		if (obj instanceof String) {
			onMessage(ProtoHelper.parseJSON(obj.toString()));
		}
		return null;
	}

}
