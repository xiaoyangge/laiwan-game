package com.java80.littlegame.service.quartz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.config.Config;
import com.java80.littlegame.common.base.BaseHandler;
import com.java80.littlegame.common.base.BaseVerticle;
import com.java80.littlegame.common.base.Runner;
import com.java80.littlegame.common.base.ServiceStatus;
import com.java80.littlegame.common.base.SystemConsts;

import io.vertx.core.VertxOptions;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class QuartzVerticle extends BaseVerticle {
	final transient static Logger log = LoggerFactory.getLogger(QuartzVerticle.class);

	public static void main(String[] args) {
		Runner.run(QuartzVerticle.class,
				new VertxOptions()
						.setClusterManager(new HazelcastClusterManager(new Config(QuartzConfig.getInstanceName())))
						.setClustered(true));
	}

	@Override
	public void stop() throws Exception {
		super.stop();
	}

	@Override
	public BaseHandler getHandler() {
		return new QuartzHandler();
	}

	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus ss = new ServiceStatus();
		ss.setInstanceName(QuartzConfig.getInstanceName());
		ss.setServiceId(QuartzConfig.getServiceId());
		ss.setServiceQueueName(QuartzConfig.getQueueName());
		System.err.println("QuartzConfig.getQueueName():" + QuartzConfig.getQueueName());
		ss.setServiceType(SystemConsts.SERVICE_TYPE_QUARTZ);
		return ss;
	}

	@Override
	public boolean needPublishServiceStatus() {
		return true;
	}

	@Override
	public String queueName() {
		return QuartzConfig.getQueueName();
	}

}
